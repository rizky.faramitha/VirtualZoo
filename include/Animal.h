#ifndef ANIMAL_H
#define ANIMAL_H

//Class Animal dan turunannya
class Animal {
  public:
  	virtual int GetX() = 0;
  	virtual int GetY() = 0;
  	int GetHabitat();
  protected:
  	int x;
  	int y;
  	int habitat_type;
};

#endif ANIMAL_H
