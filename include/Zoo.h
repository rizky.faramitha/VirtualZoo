#inlude <cstring>

#ifndef ZOO_H
#define ZOO_H

class Zoo {
  public:
	//ctor	
	Zoo();
	Zoo(int w,int h);

	//cctor
	Zoo(const Zoo&);

	//dtor
	~Zoo();

	//Setter
	SetZooWidth(int x);
	SetZooHeight(int y);
  
  	//identity menentukan tipe dari suatu cell
  	//apabila nilai identity adalah 0, maka cell belum memiliki identity
  	//apabila nilai identity < 0, maka cell adalah suatu habitat
  	//apabila nilai identity > 0, maka cell adalah suatu facility
  	SetCell(int x, int y, int identity);

	//Getter
	int GetZooWidth();
	int GetZooHeight();
  

  private:
	int ZooWidth;
	int ZooHeight;
	Cell** sel;
};

//DEKLARASI CELL DAN TURUNANNYA
class Cell {
	public:
      //ctor
      Cell();
  	
      //cctor
      Cell(const Cell&);
  
      //dtor
      ~Cell();
  
      //operator=
      Cell& operator= (const Cell&);
  
  	protected:
      char petak;
  
};

class Habitat : public Cell {
  	public:
      //Habitat adalah LandHabitat apabila bernilai 1
      //Habitat adalah WaterHabitat apabila bernilai 2
      //Habitat adalah AirHabitat apabila bernilai 3
      char SetHabitat();
};


//DEKLARASI FACILITY DAN TURUNANNYA
class Facility : public Cell {
  	public:
      //Facility adalah Road apabila bernilai -1
      //Facility adalah Restaurant apabila bernilai -2
      //Facility adalah Park apabila bernilai -3
      char SetFacility();

};

class Road : public Facility {

};

//DEKLARASI TURUNAN ROAD : ENTRANCE DAN EXIT
class Entrance : public Road {

};

class Exit : public Road {

};


#endif
